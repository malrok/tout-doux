import 'package:go_router/go_router.dart';
import 'package:presentation/features/categories_list/pages/categories_list.page.dart';
import 'package:presentation/features/todo_detail/pages/todo_detail.page.dart';
import 'package:presentation/features/todos_list/pages/todos_list.page.dart';

final router = GoRouter(
  routes: [
    GoRoute(
      path: '/',
      builder: (context, state) => const TodosListPage(),
      routes: [
        GoRoute(
          path: 'details',
          builder: (context, state) => const TodoDetailPage(),
        ),
        GoRoute(
          path: 'details/:id',
          builder: (context, state) => TodoDetailPage(
            id: int.tryParse(state.pathParameters['id'] ?? "null"),
          ),
        ),
        GoRoute(
          path: 'categories',
          builder: (context, state) => const CategoriesList(),
        ),
      ],
    ),
  ],
);
