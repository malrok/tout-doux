import 'package:flutter/material.dart';
import 'package:l10n/l10n.dart';
import 'package:presentation/constants/theme.dart';
import 'package:todo/di/service_locator.dart';
import 'package:todo/routing/router.dart';

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      theme: MaterialTheme(Theme.of(context).textTheme).light(),
      localizationsDelegates: L10nModuleLocalizations.localizationsDelegates,
      supportedLocales: L10nModuleLocalizations.supportedLocales,
      builder: (context, child) {
        if (!getIt.isRegistered<L10nModuleLocalizations>()) {
          getIt.registerSingleton<L10nModuleLocalizations>(L10nModuleLocalizations.of(context)!);
        }
        return child ?? const SizedBox();
      },
      routerConfig: router,
    );
  }
}
