import 'package:flutter/material.dart';
import 'package:todo/di/service_locator.dart';
import 'package:todo/presentation/app.widget.dart';

void main() {
  configureDependencies();
  runApp(const App());
}
