import 'package:data/di/service_locator.dart';
import 'package:domain/di/service_locator.dart';
import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';

import 'service_locator.config.dart';

final getIt = GetIt.instance;

@InjectableInit(
  initializerName: 'init', // default
  preferRelativeImports: true, // default
  asExtension: true, // default
)
void configureDependencies() {
  getIt.init(environment: 'prod');
  configureDataModuleDependencies(getIt, 'prod');
  configureDomainModuleDependencies(getIt, 'prod');
}
