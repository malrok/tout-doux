class AppSizes {
  static double zero = 0;

  static double xxxsmall = 2;
  static double xxsmall = 4;
  static double xsmall = 8;
  static double small = 12;
  static double regular = 16;
  static double medium = 24;
  static double large = 32;
  static double xlarge = 48;
  static double xxlarge = 56;
  static double xxxlarge = 64;

  static double hugeSmall = 40;
  static double hugeMedium = 80;
  static double hugeLarge = 120;
}

class AppRadius {
  static double small = 6;
  static double medium = 8;
  static double large = 12;
  static double max = 999;
}
