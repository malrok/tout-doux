part of 'todo_detail_cubit.dart';

sealed class TodoDetailState {}

class TodoDetailInitial extends TodoDetailState {}

class TodoDetailLoading extends TodoDetailState {}

class TodoDetailLoaded extends TodoDetailState {
  final Todo todo;

  TodoDetailLoaded(this.todo);
}

class TodoDetailError extends TodoDetailState {
  final String error;

  TodoDetailError(this.error);
}
