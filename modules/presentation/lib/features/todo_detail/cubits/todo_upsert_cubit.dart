import 'package:domain/entities/failure.entity.dart';
import 'package:domain/entities/todo.entity.dart';
import 'package:domain/use_cases/upsert_todo.use_case.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fpdart/fpdart.dart';

part 'todo_upsert_state.dart';

class TodoUpsertCubit extends Cubit<TodoUpsertState> {
  final UpsertTodoUseCase _upsertTodoUseCase;

  TodoUpsertCubit(this._upsertTodoUseCase) : super(TodoUpsertInitial());

  void upsert(Todo todo) => _upsertTodoUseCase.execute(todo).then(
        (Either<Failure, Todo> value) => value.match(
          (Failure error) => emit(TodoUpsertError(error.message)),
          (Todo todo) => emit(TodoUpsertLoaded(todo)),
        ),
      );
}
