import 'package:domain/entities/failure.entity.dart';
import 'package:domain/entities/todo.entity.dart';
import 'package:domain/use_cases/get_todo_by_id.use_case.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fpdart/fpdart.dart';

part 'todo_detail_state.dart';

class TodoDetailCubit extends Cubit<TodoDetailState> {
  final GetTodoByIdUseCase _getTodoByIdUseCase;

  TodoDetailCubit(this._getTodoByIdUseCase) : super(TodoDetailInitial());

  void loadTodo(int? id) {
    _getTodoByIdUseCase.execute(id).then((Either<Failure, Todo> event) {
      event.match(
        (Failure error) => emit(TodoDetailError(error.message)),
        (Todo todo) => emit(TodoDetailLoaded(todo)),
      );
    });
  }
}
