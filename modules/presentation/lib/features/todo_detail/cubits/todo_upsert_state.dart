part of 'todo_upsert_cubit.dart';

sealed class TodoUpsertState {}

class TodoUpsertInitial extends TodoUpsertState {}

class TodoUpsertLoading extends TodoUpsertState {}

class TodoUpsertLoaded extends TodoUpsertState {
  final Todo todo;

  TodoUpsertLoaded(this.todo);
}

class TodoUpsertError extends TodoUpsertState {
  final String error;

  TodoUpsertError(this.error);
}
