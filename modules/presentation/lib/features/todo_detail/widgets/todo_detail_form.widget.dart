import 'package:domain/entities/category.entity.dart';
import 'package:domain/entities/todo.entity.dart';
import 'package:domain/repositories/category.repository.dart';
import 'package:domain/repositories/todo.repository.dart';
import 'package:domain/use_cases/get_all_categories.use_case.dart';
import 'package:domain/use_cases/upsert_todo.use_case.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get_it/get_it.dart';
import 'package:go_router/go_router.dart';
import 'package:l10n/l10n.dart';
import 'package:presentation/constants/dimensions.dart';
import 'package:presentation/features/categories_list/cubits/categories_list_cubit.dart';
import 'package:presentation/features/todo_detail/cubits/todo_upsert_cubit.dart';

class TodoDetailForm extends StatefulWidget {
  final Todo todo;

  const TodoDetailForm({super.key, required this.todo});

  @override
  State<TodoDetailForm> createState() => _TodoDetailFormState();
}

class _TodoDetailFormState extends State<TodoDetailForm> {
  final _formKey = GlobalKey<FormBuilderState>();

  Todo? newTodo;

  final TodoUpsertCubit _cubit = TodoUpsertCubit(UpsertTodoUseCase(GetIt.instance<TodoRepository>()));

  @override
  void didChangeDependencies() {
    newTodo = widget.todo.copyWith();
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(AppSizes.regular),
      child: BlocConsumer<TodoUpsertCubit, TodoUpsertState>(
        bloc: _cubit,
        listener: (context, state) {
          switch (state) {
            case TodoUpsertLoaded():
              Navigator.of(context).pop();
            case TodoUpsertError():
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('an error occurred: ${state.error}')));
            default:
              break;
          }
        },
        builder: (context, state) => FormBuilder(
          key: _formKey,
          child: Column(
            children: [
              FormBuilderTextField(
                name: 'title',
                initialValue: widget.todo.title,
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(),
                ]),
                onChanged: (val) => newTodo = newTodo!.copyWith(title: val),
              ),
              FormBuilderTextField(
                name: 'description',
                initialValue: widget.todo.description,
                onChanged: (val) => newTodo = newTodo!.copyWith(description: val),
              ),
              FormBuilderTextField(
                name: 'duration',
                initialValue: (widget.todo.duration ?? 0).toString(),
                keyboardType: TextInputType.number,
                onChanged: (val) {
                  if (val != null) {
                    newTodo = newTodo!.copyWith(duration: int.parse(val));
                  }
                },
              ),
              FormBuilderDateTimePicker(
                name: 'end',
                initialValue: widget.todo.end,
                onChanged: (val) => newTodo = newTodo!.copyWith(end: val),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(),
                ]),
              ),
              FormBuilderCheckbox(
                title: Text(l10n.todo_detail_form_completed_label),
                name: 'completed',
                initialValue: widget.todo.completed,
                onChanged: (val) => newTodo = newTodo!.copyWith(completed: val),
              ),
              BlocBuilder<CategoriesListCubit, CategoriesListState>(
                bloc: CategoriesListCubit(GetAllCategoriesUseCase(GetIt.instance<CategoryRepository>()))..fetchAll(),
                builder: (context, state) => switch (state) {
                  CategoriesListPending() => const CircularProgressIndicator(),
                  CategoriesListLoaded() => Row(
                      children: [
                        Expanded(
                          child: FormBuilderDropdown<Category>(
                            name: 'category',
                            initialValue: widget.todo.category,
                            items: state.categories
                                .map(
                                  (category) => DropdownMenuItem(
                                    value: category,
                                    child: Text(category.name),
                                  ),
                                )
                                .toList(),
                            onChanged: (val) => newTodo = newTodo!.copyWith(category: val),
                          ),
                        ),
                        IconButton(
                          onPressed: () => context.push('/categories'),
                          icon: const Icon(Icons.add),
                        ),
                      ],
                    ),
                  CategoriesListError() => const Text('an error occurred'),
                },
              ),
              FilledButton(
                onPressed: () {
                  _formKey.currentState?.saveAndValidate();
                  if (_formKey.currentState?.isValid ?? false) {
                    _cubit.upsert(newTodo!);
                  }
                },
                child: Text(l10n.todo_detail_form_save_label),
              )
            ],
          ),
        ),
      ),
    );
  }
}
