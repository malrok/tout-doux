import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:presentation/features/todo_detail/cubits/todo_detail_cubit.dart';
import 'package:presentation/features/todo_detail/widgets/todo_detail_form.widget.dart';

class TodoDetail extends StatelessWidget {
  const TodoDetail({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TodoDetailCubit, TodoDetailState>(
      builder: (context, state) => switch (state) {
        TodoDetailInitial() || TodoDetailLoading() => const Center(child: CircularProgressIndicator()),
        TodoDetailLoaded() => TodoDetailForm(todo: state.todo),
        TodoDetailError() => Center(child: Text(state.error)),
      },
    );
  }
}
