import 'package:domain/repositories/todo.repository.dart';
import 'package:domain/use_cases/get_todo_by_id.use_case.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:l10n/l10n.dart';
import 'package:presentation/features/todo_detail/cubits/todo_detail_cubit.dart';
import 'package:presentation/features/todo_detail/widgets/todo_detail.widget.dart';

class TodoDetailPage extends StatelessWidget {
  final int? id;

  const TodoDetailPage({super.key, this.id});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(l10n.todo_detail_page_title)),
      body: BlocProvider(
        create: (_) => TodoDetailCubit(GetTodoByIdUseCase(GetIt.instance<TodoRepository>()))..loadTodo(id),
        child: const TodoDetail(),
      ),
    );
  }
}
