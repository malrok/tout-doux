import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:l10n/l10n.dart';
import 'package:presentation/features/todos_list/cubits/todos_list_cubit.dart';
import 'package:presentation/features/todos_list/widgets/todos_list_item.widget.dart';

class TodosList extends StatelessWidget {
  const TodosList({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TodosListCubit, TodosListState>(builder: (BuildContext context, TodosListState state) {
      switch (state) {
        case TodosListInitial():
        case TodosListLoading():
          return const Center(
            child: CircularProgressIndicator(),
          );
        case TodosListLoaded():
          if (state.todos.isEmpty) {
            return Center(child: Text(l10n.no_data));
          }
          return ListView.builder(
            itemBuilder: (context, index) {
              return TodosListItem(todo: state.todos[index]);
            },
            itemCount: state.todos.length,
          );
        case TodosListError():
          return Center(child: Text(state.error));
      }
    });
  }
}
