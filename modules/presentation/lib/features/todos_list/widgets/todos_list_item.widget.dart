import 'package:domain/entities/todo.entity.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:presentation/constants/dimensions.dart';

class TodosListItem extends StatelessWidget {
  final Todo todo;

  const TodosListItem({super.key, required this.todo});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => context.go('/details/${todo.id}'),
      child: Padding(
        padding: EdgeInsets.all(AppSizes.regular),
        child: Text(todo.title, style: Theme.of(context).textTheme.headlineSmall),
      ),
    );
  }
}
