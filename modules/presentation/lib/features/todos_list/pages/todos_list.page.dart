import 'package:domain/repositories/todo.repository.dart';
import 'package:domain/use_cases/get_all_todos.use_case.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:go_router/go_router.dart';
import 'package:l10n/l10n.dart';
import 'package:presentation/features/todos_list/cubits/todos_list_cubit.dart';
import 'package:presentation/features/todos_list/widgets/todos_list.widget.dart';

class TodosListPage extends StatelessWidget {
  const TodosListPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(l10n.todos_list_page_title)),
      body: BlocProvider(
        create: (_) => TodosListCubit(GetAllTodosUseCase(GetIt.instance<TodoRepository>()))..fetchAll(),
        child: const TodosList(),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => context.go('/details'),
        child: const Icon(Icons.add, color: Colors.white, size: 28),
      ),
    );
  }
}
