part of 'todos_list_cubit.dart';

sealed class TodosListState {}

class TodosListInitial extends TodosListState {}

class TodosListLoading extends TodosListState {}

class TodosListLoaded extends TodosListState {
  final List<Todo> todos;

  TodosListLoaded(this.todos);
}

class TodosListError extends TodosListState {
  final String error;

  TodosListError(this.error);
}
