import 'package:domain/entities/failure.entity.dart';
import 'package:domain/entities/todo.entity.dart';
import 'package:domain/use_cases/get_all_todos.use_case.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fpdart/fpdart.dart';

part 'todos_list_state.dart';

class TodosListCubit extends Cubit<TodosListState> {
  final GetAllTodosUseCase _getAllTodosUseCase;

  TodosListCubit(this._getAllTodosUseCase) : super(TodosListInitial());

  void fetchAll() => _getAllTodosUseCase.execute().listen(
        (Either<Failure, List<Todo>> event) {
          event.match(
            (Failure error) => emit(TodosListError(error.message)),
            (List<Todo> todos) => emit(TodosListLoaded(todos)),
          );
        },
      );
}
