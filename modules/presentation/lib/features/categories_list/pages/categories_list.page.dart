import 'package:domain/entities/category.entity.dart';
import 'package:domain/repositories/category.repository.dart';
import 'package:domain/use_cases/get_all_categories.use_case.dart';
import 'package:domain/use_cases/upsert_category.use_case.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get_it/get_it.dart';
import 'package:l10n/l10n.dart';
import 'package:presentation/constants/dimensions.dart';
import 'package:presentation/features/categories_list/cubits/categories_list_cubit.dart';
import 'package:presentation/features/categories_list/cubits/category_upsert_cubit.dart';

class CategoriesList extends StatelessWidget {
  const CategoriesList({super.key});

  @override
  Widget build(BuildContext context) {
    final formKey = GlobalKey<FormBuilderState>();

    return Scaffold(
      appBar: AppBar(title: Text(l10n.todo_detail_page_title)),
      body: Padding(
        padding: EdgeInsets.all(AppSizes.regular),
        child: Column(
          children: [
            FormBuilder(
              key: formKey,
              child: Row(
                children: [
                  Expanded(
                    child: FormBuilderTextField(
                      name: 'name',
                      initialValue: '',
                      validator: FormBuilderValidators.compose([
                        FormBuilderValidators.required(),
                      ]),
                    ),
                  ),
                  FilledButton(
                    onPressed: () {
                      formKey.currentState?.saveAndValidate();
                      if (formKey.currentState?.isValid ?? false) {
                        CategoryUpsertCubit(UpsertCategoryUseCase(GetIt.instance<CategoryRepository>()))
                            .upsert(Category(name: formKey.currentState?.fields['name']?.value!));
                      }
                    },
                    child: Text(l10n.todo_detail_form_save_label),
                  )
                ],
              ),
            ),
            BlocBuilder<CategoriesListCubit, CategoriesListState>(
              bloc: CategoriesListCubit(GetAllCategoriesUseCase(GetIt.instance<CategoryRepository>()))..fetchAll(),
              builder: (context, state) {
                return switch (state) {
                  CategoriesListPending() => const CircularProgressIndicator(),
                  CategoriesListLoaded() => ListView.builder(
                      shrinkWrap: true,
                      itemCount: state.categories.length,
                      itemBuilder: (context, index) {
                        return Text(state.categories[index].name);
                      },
                    ),
                  CategoriesListError() => Text('an error occurred ${state.error}'),
                };
              },
            ),
          ],
        ),
      ),
    );
  }
}
