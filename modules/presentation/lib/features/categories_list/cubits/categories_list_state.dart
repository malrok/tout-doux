part of 'categories_list_cubit.dart';

sealed class CategoriesListState {}

class CategoriesListPending extends CategoriesListState {}

class CategoriesListLoaded extends CategoriesListState {
  final List<Category> categories;

  CategoriesListLoaded(this.categories);
}

class CategoriesListError extends CategoriesListState {
  final String error;

  CategoriesListError(this.error);
}
