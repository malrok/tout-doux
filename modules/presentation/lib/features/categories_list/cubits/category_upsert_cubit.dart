import 'package:domain/entities/failure.entity.dart';
import 'package:domain/entities/category.entity.dart';
import 'package:domain/use_cases/upsert_category.use_case.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fpdart/fpdart.dart';

part 'category_upsert_state.dart';

class CategoryUpsertCubit extends Cubit<CategoryUpsertState> {
  final UpsertCategoryUseCase _upsertCategoryUseCase;

  CategoryUpsertCubit(this._upsertCategoryUseCase) : super(CategoryUpsertInitial());

  void upsert(Category category) => _upsertCategoryUseCase.execute(category).then(
        (Either<Failure, Category> value) => value.match(
          (Failure error) => emit(CategoryUpsertError(error.message)),
          (Category category) => emit(CategoryUpsertLoaded(category)),
    ),
  );
}
