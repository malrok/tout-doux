import 'package:domain/entities/category.entity.dart';
import 'package:domain/entities/failure.entity.dart';
import 'package:domain/use_cases/get_all_categories.use_case.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fpdart/fpdart.dart';

part 'categories_list_state.dart';

class CategoriesListCubit extends Cubit<CategoriesListState> {
  final GetAllCategoriesUseCase _getAllCategoriesUseCase;

  CategoriesListCubit(this._getAllCategoriesUseCase) : super(CategoriesListPending());

  void fetchAll() => _getAllCategoriesUseCase.execute().listen(
        (Either<Failure, List<Category>> event) {
          event.match(
            (Failure error) => emit(CategoriesListError(error.message)),
            (List<Category> todos) => emit(CategoriesListLoaded(todos)),
          );
        },
      );
}
