part of 'category_upsert_cubit.dart';

sealed class CategoryUpsertState {}

class CategoryUpsertInitial extends CategoryUpsertState {}

class CategoryUpsertLoading extends CategoryUpsertState {}

class CategoryUpsertLoaded extends CategoryUpsertState {
  final Category category;

  CategoryUpsertLoaded(this.category);
}

class CategoryUpsertError extends CategoryUpsertState {
  final String error;

  CategoryUpsertError(this.error);
}
