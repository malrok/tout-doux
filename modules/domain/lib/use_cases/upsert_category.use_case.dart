import 'dart:async';

import 'package:domain/entities/category.entity.dart';
import 'package:domain/entities/failure.entity.dart';
import 'package:domain/repositories/category.repository.dart';
import 'package:fpdart/fpdart.dart';

class UpsertCategoryUseCase {
  final CategoryRepository _categoryRepository;

  UpsertCategoryUseCase(this._categoryRepository);

  Future<Either<Failure, Category>> execute(Category category) async {
    try {
      final res = await _categoryRepository.upsert(category);
      return Either.of(res);
    } catch (error) {
      return Either.left(Failure(error.toString()));
    }
  }
}
