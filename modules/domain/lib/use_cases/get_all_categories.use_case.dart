import 'dart:async';

import 'package:domain/entities/category.entity.dart';
import 'package:domain/entities/failure.entity.dart';
import 'package:domain/repositories/category.repository.dart';
import 'package:fpdart/fpdart.dart';

class GetAllCategoriesUseCase {
  final CategoryRepository _categoryRepository;

  GetAllCategoriesUseCase(this._categoryRepository);

  Stream<Either<Failure, List<Category>>> execute() {
    StreamController<Either<Failure, List<Category>>> controller = StreamController();

    runZonedGuarded(
          () => _categoryRepository.getAll().listen((todos) => controller.add(Either<Failure, List<Category>>.of(todos))),
          (error, stack) => controller.add(Either<Failure, List<Category>>.left(Failure(error.toString()))),
    );

    return controller.stream;
  }
}
