import 'dart:async';

import 'package:domain/entities/failure.entity.dart';
import 'package:domain/entities/todo.entity.dart';
import 'package:domain/repositories/todo.repository.dart';
import 'package:fpdart/fpdart.dart';

class UpsertTodoUseCase {
  final TodoRepository _todoRepository;

  UpsertTodoUseCase(this._todoRepository);

  Future<Either<Failure, Todo>> execute(Todo todo) async {
    try {
      final res = (todo.id == null) ? await _todoRepository.add(todo) : await _todoRepository.set(todo);
      return Either.of(res);
    } catch (error) {
      return Either.left(Failure(error.toString()));
    }
  }
}
