import 'dart:async';

import 'package:domain/entities/failure.entity.dart';
import 'package:domain/entities/todo.entity.dart';
import 'package:domain/repositories/todo.repository.dart';
import 'package:fpdart/fpdart.dart';

class GetAllTodosUseCase {
  final TodoRepository _todoRepository;

  GetAllTodosUseCase(this._todoRepository);

  Stream<Either<Failure, List<Todo>>> execute() {
    StreamController<Either<Failure, List<Todo>>> controller = StreamController();

    runZonedGuarded(
      () => _todoRepository.getAll().listen((todos) => controller.add(Either<Failure, List<Todo>>.of(todos))),
      (error, stack) => controller.add(Either<Failure, List<Todo>>.left(Failure(error.toString()))),
    );

    return controller.stream;
  }
}
