import 'dart:async';

import 'package:domain/entities/failure.entity.dart';
import 'package:domain/entities/todo.entity.dart';
import 'package:domain/repositories/todo.repository.dart';
import 'package:fpdart/fpdart.dart';

class GetTodoByIdUseCase {
  final TodoRepository _todoRepository;

  GetTodoByIdUseCase(this._todoRepository);

  Future<Either<Failure, Todo>> execute(int? id) async {
    if (id == null) return Either.of(Todo.empty());

    try {
      final todo = await _todoRepository.getById(id);
      return Either.of(todo);
    } catch (error) {
      return Either.left(Failure(error.toString()));
    }
  }
}
