import 'package:copy_with_extension/copy_with_extension.dart';

part 'category.entity.g.dart';

@CopyWith()
class Category {
  final String name;

  Category({required this.name});

  @override
  bool operator ==(other) {
    if (other is! Category) return false;
    return other.name == name;
  }

  @override
  int get hashCode => super.hashCode;
}
