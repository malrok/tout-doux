import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:domain/entities/category.entity.dart';

part 'todo.entity.g.dart';

@CopyWith()
class Todo {
  final int? id;
  final String title;
  final String? description;
  final int? duration;
  final DateTime end;
  final bool completed;
  final Category category;

  Todo({
    this.id,
    required this.title,
    this.description,
    this.duration,
    required this.end,
    this.completed = false,
    required this.category,
  });

  Todo.empty()
      : id = null,
        title = '',
        description = '',
        duration = null,
        end = DateTime.now(),
        completed = false,
        category = Category(name: 'default');
}
