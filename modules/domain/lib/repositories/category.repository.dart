import 'package:domain/entities/category.entity.dart';

abstract class CategoryRepository {
  Stream<List<Category>> getAll();

  Future<Category> upsert(Category todo);

  Future<Category> getByName(String name);
}