import 'package:domain/entities/todo.entity.dart';

abstract class TodoRepository {
  Stream<List<Todo>> getAll();

  Future<Todo> add(Todo todo);

  Future<Todo> set(Todo todo);

  Future<Todo> getById(int id);
}
