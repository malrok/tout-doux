import 'package:domain/di/service_locator.dart';
import 'package:domain/entities/category.entity.dart';
import 'package:domain/entities/failure.entity.dart';
import 'package:domain/entities/todo.entity.dart';
import 'package:domain/repositories/todo.repository.dart';
import 'package:domain/use_cases/get_todo_by_id.use_case.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:fpdart/fpdart.dart';
import 'package:get_it/get_it.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'get_all_todos_use_case_test.mocks.dart';

@GenerateNiceMocks([MockSpec<TodoRepository>()])
void main() {
  final GetIt getIt = GetIt.instance;
  final mockTodoRepository = MockTodoRepository();

  setUpAll(() async {
    TestWidgetsFlutterBinding.ensureInitialized();
    configureDomainModuleDependencies(getIt, 'test');
  });

  group("getTodoByIdUseCase", () {
    test("can get with data", () async {
      when(mockTodoRepository.getById(1)).thenAnswer((_) => Future.value(Todo(
            id: 1,
            title: 'test',
            end: DateTime.now().add(Duration(days: 5)),
            category: Category(name: 'recipe'),
          )));

      final todos = await GetTodoByIdUseCase(mockTodoRepository).execute(1);
      expect(todos.isRight(), true);
      final data = todos.getRight().getOrElse(() => Todo.empty());
      expect(data.id, 1);
      expect(data.title, equals('test'));
      expect(data.category.name, equals('recipe'));
    });
    test("can get with exception", () async {
      when(mockTodoRepository.getById(-1)).thenThrow(Exception('an error occurred'));

      final todos = await GetTodoByIdUseCase(mockTodoRepository).execute(-1);
      expect(todos.isLeft(), true);
      expect(todos.getLeft().getOrElse(() => Failure('')).message, equals('Exception: an error occurred'));
    });
  });
}
