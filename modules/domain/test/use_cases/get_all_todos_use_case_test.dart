import 'package:domain/di/service_locator.dart';
import 'package:domain/entities/category.entity.dart';
import 'package:domain/entities/failure.entity.dart';
import 'package:domain/entities/todo.entity.dart';
import 'package:domain/repositories/todo.repository.dart';
import 'package:domain/use_cases/get_all_todos.use_case.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:fpdart/fpdart.dart';
import 'package:get_it/get_it.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'get_all_todos_use_case_test.mocks.dart';

@GenerateNiceMocks([MockSpec<TodoRepository>()])
void main() {
  final GetIt getIt = GetIt.instance;
  final mockTodoRepository = MockTodoRepository();

  setUpAll(() async {
    TestWidgetsFlutterBinding.ensureInitialized();
    configureDomainModuleDependencies(getIt, 'test');
  });

  group("getAllTodosUseCase", () {
    test("can get all with data", () async {
      when(mockTodoRepository.getAll()).thenAnswer((_) => Stream.value([
            Todo(
              title: 'test',
              end: DateTime.now().add(Duration(days: 5)),
              category: Category(name: 'recipe'),
            )
          ]));

      final todos = await GetAllTodosUseCase(mockTodoRepository).execute().first;
      expect(todos.isRight(), true);
      final data = todos.getRight().getOrElse(() => []);
      expect(data.length, 1);
      expect(data[0].title, equals('test'));
      expect(data[0].category.name, equals('recipe'));
    });
    test("can get all with exception", () async {
      when(mockTodoRepository.getAll()).thenThrow(Exception('an error occurred'));

      final todos = await GetAllTodosUseCase(mockTodoRepository).execute().first;
      expect(todos.isLeft(), true);
      expect(todos.getLeft().getOrElse(() => Failure('')).message, equals('Exception: an error occurred'));
    });
  });
}
