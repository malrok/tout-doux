library l10n;

import 'package:get_it/get_it.dart';
import 'package:l10n/l10n.dart';

export 'src/l10n/generated/l10n_module_localizations.dart';

final l10n = GetIt.instance.get<L10nModuleLocalizations>();