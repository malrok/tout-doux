import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:intl/intl.dart' as intl;

import 'l10n_module_localizations_en.dart';

/// Callers can lookup localized strings with an instance of L10nModuleLocalizations
/// returned by `L10nModuleLocalizations.of(context)`.
///
/// Applications need to include `L10nModuleLocalizations.delegate()` in their app's
/// `localizationDelegates` list, and the locales they support in the app's
/// `supportedLocales` list. For example:
///
/// ```dart
/// import 'generated/l10n_module_localizations.dart';
///
/// return MaterialApp(
///   localizationsDelegates: L10nModuleLocalizations.localizationsDelegates,
///   supportedLocales: L10nModuleLocalizations.supportedLocales,
///   home: MyApplicationHome(),
/// );
/// ```
///
/// ## Update pubspec.yaml
///
/// Please make sure to update your pubspec.yaml to include the following
/// packages:
///
/// ```yaml
/// dependencies:
///   # Internationalization support.
///   flutter_localizations:
///     sdk: flutter
///   intl: any # Use the pinned version from flutter_localizations
///
///   # Rest of dependencies
/// ```
///
/// ## iOS Applications
///
/// iOS applications define key application metadata, including supported
/// locales, in an Info.plist file that is built into the application bundle.
/// To configure the locales supported by your app, you’ll need to edit this
/// file.
///
/// First, open your project’s ios/Runner.xcworkspace Xcode workspace file.
/// Then, in the Project Navigator, open the Info.plist file under the Runner
/// project’s Runner folder.
///
/// Next, select the Information Property List item, select Add Item from the
/// Editor menu, then select Localizations from the pop-up menu.
///
/// Select and expand the newly-created Localizations item then, for each
/// locale your application supports, add a new item and select the locale
/// you wish to add from the pop-up menu in the Value field. This list should
/// be consistent with the languages listed in the L10nModuleLocalizations.supportedLocales
/// property.
abstract class L10nModuleLocalizations {
  L10nModuleLocalizations(String locale) : localeName = intl.Intl.canonicalizedLocale(locale.toString());

  final String localeName;

  static L10nModuleLocalizations? of(BuildContext context) {
    return Localizations.of<L10nModuleLocalizations>(context, L10nModuleLocalizations);
  }

  static const LocalizationsDelegate<L10nModuleLocalizations> delegate = _L10nModuleLocalizationsDelegate();

  /// A list of this localizations delegate along with the default localizations
  /// delegates.
  ///
  /// Returns a list of localizations delegates containing this delegate along with
  /// GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate,
  /// and GlobalWidgetsLocalizations.delegate.
  ///
  /// Additional delegates can be added by appending to this list in
  /// MaterialApp. This list does not have to be used at all if a custom list
  /// of delegates is preferred or required.
  static const List<LocalizationsDelegate<dynamic>> localizationsDelegates = <LocalizationsDelegate<dynamic>>[
    delegate,
    GlobalMaterialLocalizations.delegate,
    GlobalCupertinoLocalizations.delegate,
    GlobalWidgetsLocalizations.delegate,
  ];

  /// A list of this localizations delegate's supported locales.
  static const List<Locale> supportedLocales = <Locale>[
    Locale('en')
  ];

  /// No description provided for @no_data.
  ///
  /// In en, this message translates to:
  /// **'No data'**
  String get no_data;

  /// No description provided for @todos_list_page_title.
  ///
  /// In en, this message translates to:
  /// **'Todos'**
  String get todos_list_page_title;

  /// No description provided for @todo_detail_page_title.
  ///
  /// In en, this message translates to:
  /// **'Details'**
  String get todo_detail_page_title;

  /// No description provided for @todo_detail_form_completed_label.
  ///
  /// In en, this message translates to:
  /// **'Completed'**
  String get todo_detail_form_completed_label;

  /// No description provided for @todo_detail_form_save_label.
  ///
  /// In en, this message translates to:
  /// **'Save'**
  String get todo_detail_form_save_label;
}

class _L10nModuleLocalizationsDelegate extends LocalizationsDelegate<L10nModuleLocalizations> {
  const _L10nModuleLocalizationsDelegate();

  @override
  Future<L10nModuleLocalizations> load(Locale locale) {
    return SynchronousFuture<L10nModuleLocalizations>(lookupL10nModuleLocalizations(locale));
  }

  @override
  bool isSupported(Locale locale) => <String>['en'].contains(locale.languageCode);

  @override
  bool shouldReload(_L10nModuleLocalizationsDelegate old) => false;
}

L10nModuleLocalizations lookupL10nModuleLocalizations(Locale locale) {


  // Lookup logic when only language code is specified.
  switch (locale.languageCode) {
    case 'en': return L10nModuleLocalizationsEn();
  }

  throw FlutterError(
    'L10nModuleLocalizations.delegate failed to load unsupported locale "$locale". This is likely '
    'an issue with the localizations generation tool. Please file an issue '
    'on GitHub with a reproducible sample app and the gen-l10n configuration '
    'that was used.'
  );
}
