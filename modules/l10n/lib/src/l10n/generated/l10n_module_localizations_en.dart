import 'l10n_module_localizations.dart';

/// The translations for English (`en`).
class L10nModuleLocalizationsEn extends L10nModuleLocalizations {
  L10nModuleLocalizationsEn([String locale = 'en']) : super(locale);

  @override
  String get no_data => 'No data';

  @override
  String get todos_list_page_title => 'Todos';

  @override
  String get todo_detail_page_title => 'Details';

  @override
  String get todo_detail_form_completed_label => 'Completed';

  @override
  String get todo_detail_form_save_label => 'Save';
}
