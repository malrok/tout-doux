import 'package:data/di/service_locator.dart';
import 'package:domain/entities/category.entity.dart';
import 'package:domain/entities/todo.entity.dart';
import 'package:domain/repositories/todo.repository.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';

void main() {
  final GetIt getIt = GetIt.instance;

  setUpAll(() async {
    TestWidgetsFlutterBinding.ensureInitialized();
    configureDataModuleDependencies(getIt, 'test');
  });

  group("todoRepository", () {
    test("can get all empty", () async {
      final todos = await getIt<TodoRepository>().getAll().first;
      expect(todos.isNotEmpty, false);
    });
    test("can insert data in todo table", () async {
      final todo = await getIt<TodoRepository>().add(Todo(
        title: 'test',
        end: DateTime.now().add(Duration(days: 5)),
        category: Category(name: 'recipe')
      ));
      expect(todo.id, isNotNull);
    });
    test("can get all with data", () async {
      final todos = await getIt<TodoRepository>().getAll().first;
      expect(todos.length, 1);
      expect(todos[0].title, equals('test'));
    });
  });
}
