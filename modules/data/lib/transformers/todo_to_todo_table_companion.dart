import 'package:data/datasources/local/database.dart';
import 'package:domain/entities/todo.entity.dart';
import 'package:drift/drift.dart';

TodoTableCompanion todoToDataTableCompanion(Todo todo) => TodoTableCompanion(
      id: todo.id != null ? Value(todo.id!) : Value.absent(),
      title: Value(todo.title),
      description: Value(todo.description),
      duration: Value(todo.duration),
      end: Value(todo.end),
      completed: Value(todo.completed),
      category: Value(todo.category.name),
    );
