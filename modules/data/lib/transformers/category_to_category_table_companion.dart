import 'package:data/datasources/local/database.dart';
import 'package:domain/entities/category.entity.dart';
import 'package:drift/drift.dart';

CategoryTableCompanion categoryToDataTableCompanion(Category category) =>
    CategoryTableCompanion(name: Value(category.name));
