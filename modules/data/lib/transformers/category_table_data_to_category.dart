import 'package:data/datasources/local/database.dart';
import 'package:domain/entities/category.entity.dart';

Category categoryTableDataToCategory(CategoryTableData data) => Category(name: data.name);
