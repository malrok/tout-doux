import 'package:data/datasources/local/database.dart';
import 'package:data/transformers/category_table_data_to_category.dart';
import 'package:domain/entities/todo.entity.dart';

Todo todoTableDataToTodo(TodoTableData todoData, CategoryTableData categoryData) => Todo(
      id: todoData.id,
      title: todoData.title,
      description: todoData.description,
      duration: todoData.duration,
      end: todoData.end,
      completed: todoData.completed,
      category: categoryTableDataToCategory(categoryData),
    );
