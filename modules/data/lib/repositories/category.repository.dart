import 'package:data/datasources/local/category.dao.dart';
import 'package:data/datasources/local/database.dart';
import 'package:data/transformers/category_table_data_to_category.dart';
import 'package:data/transformers/category_to_category_table_companion.dart';
import 'package:domain/entities/category.entity.dart';
import 'package:domain/repositories/category.repository.dart';
import 'package:injectable/injectable.dart';

@Injectable(as: CategoryRepository)
class CategoryRepositoryImpl implements CategoryRepository {
  final CategoryDao _categoryDao;

  CategoryRepositoryImpl(this._categoryDao);

  @override
  Stream<List<Category>> getAll() => _categoryDao.getAll().map((List<CategoryTableData> list) =>
      list.map((CategoryTableData data) => categoryTableDataToCategory(data)).toList());

  @override
  Future<Category> upsert(Category category) async {
    await _categoryDao.upsert(categoryToDataTableCompanion(category));
    return category;
  }

  @override
  Future<Category> getByName(String name) async {
    final category = await _categoryDao.getByName(name);

    return categoryTableDataToCategory(category);
  }
}
