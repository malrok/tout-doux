import 'dart:async';

import 'package:data/datasources/local/category.dao.dart';
import 'package:data/datasources/local/database.dart';
import 'package:data/datasources/local/todo.dao.dart';
import 'package:data/transformers/todo_table_data_to_todo.dart';
import 'package:data/transformers/todo_to_todo_table_companion.dart';
import 'package:domain/entities/todo.entity.dart';
import 'package:domain/repositories/todo.repository.dart';
import 'package:injectable/injectable.dart';

@Injectable(as: TodoRepository)
class TodoRepositoryImpl implements TodoRepository {
  final TodoDao _todoDao;
  final CategoryDao _categoryDao;

  TodoRepositoryImpl(this._todoDao, this._categoryDao);

  @override
  Stream<List<Todo>> getAll() {
    return _todoDao.getAll().asyncMap((List<TodoTableData> list) async {
      final List<Todo> newList = [];
      for (final todo in list) {
        final category = await _categoryDao.getByName(todo.category);
        newList.add(todoTableDataToTodo(todo, category));
      }
      return newList;
    });
  }

  @override
  Future<Todo> add(Todo todo) async {
    final id = await _todoDao.add(todoToDataTableCompanion(todo));
    return todo.copyWith(id: id);
  }

  @override
  Future<Todo> set(Todo todo) async {
    if (await _todoDao.set(todoToDataTableCompanion(todo))) {
      return todo;
    } else {
      throw Exception('an error occurred');
    }
  }

  @override
  Future<Todo> getById(int id) async {
    final todo = await _todoDao.getById(id);

    final category = await _categoryDao.getByName(todo.category);

    return todoTableDataToTodo(todo, category);
  }
}
