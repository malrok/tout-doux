import 'package:data/datasources/local/category.table.dart';
import 'package:drift/drift.dart';

class TodoTable extends Table {
  IntColumn get id => integer().autoIncrement()();

  TextColumn get title => text()();

  TextColumn get description => text().nullable()();

  IntColumn get duration => integer().nullable()();

  DateTimeColumn get end => dateTime()();

  BoolColumn get completed => boolean()();

  TextColumn get category => text().references(CategoryTable, #name)();
}
