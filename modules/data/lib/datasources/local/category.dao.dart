import 'package:data/datasources/local/category.table.dart';
import 'package:data/datasources/local/database.dart';
import 'package:drift/drift.dart';
import 'package:injectable/injectable.dart';

part 'category.dao.g.dart';

@singleton
@DriftAccessor(tables: [CategoryTable])
class CategoryDao extends DatabaseAccessor<AppDatabase> with _$CategoryDaoMixin {
  CategoryDao(AppDatabase db) : super(db);

  Stream<List<CategoryTableData>> getAll() => select(categoryTable).watch();

  Future<int> upsert(CategoryTableCompanion data) => into(categoryTable).insertOnConflictUpdate(data);

  Future<CategoryTableData> getByName(String name) =>
      (select(categoryTable)..where((tbl) => tbl.name.equals(name))).getSingle();
}
