import 'package:data/datasources/local/database.dart';
import 'package:data/datasources/local/todo.table.dart';
import 'package:drift/drift.dart';
import 'package:injectable/injectable.dart';

part 'todo.dao.g.dart';

@singleton
@DriftAccessor(tables: [TodoTable])
class TodoDao extends DatabaseAccessor<AppDatabase> with _$TodoDaoMixin {
  TodoDao(AppDatabase db) : super(db);

  Stream<List<TodoTableData>> getAll() => select(todoTable).watch();

  Future<int> add(TodoTableCompanion data) => into(todoTable).insert(data);

  Future<bool> set(TodoTableCompanion data) => update(todoTable).replace(data);

  Future<TodoTableData> getById(int id) => (select(todoTable)..where((tbl) => tbl.id.equals(id))).getSingle();
}
