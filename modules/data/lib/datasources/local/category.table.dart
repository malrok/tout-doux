import 'package:drift/drift.dart';

class CategoryTable extends Table {
  TextColumn get name => text()();

  @override
  Set<Column> get primaryKey => {name};
}
