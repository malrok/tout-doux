// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'todo.dao.dart';

// ignore_for_file: type=lint
mixin _$TodoDaoMixin on DatabaseAccessor<AppDatabase> {
  $CategoryTableTable get categoryTable => attachedDatabase.categoryTable;
  $TodoTableTable get todoTable => attachedDatabase.todoTable;
}
