import 'package:data/di/service_locator.config.dart';
import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';

@InjectableInit(initializerName: r'$initModuleGetIt')
void configureDataModuleDependencies(GetIt getIt, [String? env]) => getIt.$initModuleGetIt(environment: env);
