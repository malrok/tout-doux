// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:data/datasources/local/category.dao.dart' as _i4;
import 'package:data/datasources/local/database.dart' as _i3;
import 'package:data/datasources/local/todo.dao.dart' as _i7;
import 'package:data/repositories/category.repository.dart' as _i6;
import 'package:data/repositories/todo.repository.dart' as _i9;
import 'package:domain/repositories/category.repository.dart' as _i5;
import 'package:domain/repositories/todo.repository.dart' as _i8;
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;

const String _prod = 'prod';
const String _test = 'test';

extension GetItInjectableX on _i1.GetIt {
// initializes the registration of main-scope dependencies inside of GetIt
  _i1.GetIt $initModuleGetIt({
    String? environment,
    _i2.EnvironmentFilter? environmentFilter,
  }) {
    final gh = _i2.GetItHelper(
      this,
      environment,
      environmentFilter,
    );
    gh.singleton<_i3.AppDatabase>(
      _i3.ProdDatabase(),
      registerFor: {_prod},
    );
    gh.singleton<_i3.AppDatabase>(
      _i3.TestDatabase(),
      registerFor: {_test},
    );
    gh.singleton<_i4.CategoryDao>(_i4.CategoryDao(gh<_i3.AppDatabase>()));
    gh.factory<_i5.CategoryRepository>(
        () => _i6.CategoryRepositoryImpl(gh<_i4.CategoryDao>()));
    gh.singleton<_i7.TodoDao>(_i7.TodoDao(gh<_i3.AppDatabase>()));
    gh.factory<_i8.TodoRepository>(() => _i9.TodoRepositoryImpl(
          gh<_i7.TodoDao>(),
          gh<_i4.CategoryDao>(),
        ));
    return this;
  }
}
